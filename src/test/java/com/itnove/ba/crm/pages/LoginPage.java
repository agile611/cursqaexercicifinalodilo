package com.itnove.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class LoginPage {

    private WebDriver driver;

    @FindBy(id = "user_name")
    public WebElement usernameTextBox;

    @FindBy(id = "user_password")
    public WebElement userpasswordTextBox;

    @FindBy(id = "bigbutton")
    public WebElement botoLogin;

    @FindBy(xpath = ".//span[@class='error'][3]")
    public WebElement errorMessage;

    @FindBy(xpath = "html/body/div[1]/div[2]/p")
    public WebElement sessionExpired;

    public void login(String user, String passwd){
        usernameTextBox.clear();
        usernameTextBox.sendKeys(user);
        userpasswordTextBox.clear();
        userpasswordTextBox.sendKeys(passwd);
        botoLogin.click();
    }

    public boolean isErrorMessagePresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.isDisplayed();
    }

    public boolean isLoginButtonPresent(WebDriver driver, WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(botoLogin));
        return botoLogin.isDisplayed();
    }

    public String errorMessageDisplayed(){
        return errorMessage.getText();
    }

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
