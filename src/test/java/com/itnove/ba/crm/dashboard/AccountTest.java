package com.itnove.crm.dashboard;

import com.itnove.ba.BaseTest;
import com.itnove.crm.pages.*;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertEquals;

public class AccountTest extends BaseTest {

    public String accountName = UUID.randomUUID().toString();

    @Test
    public void createAccount() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.createButtonClick(hover);
        dashboardPage.clickOnCreateAccountLink(hover);
        CreateAccountPage createAccountPage = new CreateAccountPage(driver);
        createAccountPage.fillName(accountName);
        createAccountPage.saveAccount();
        EditAccountPage editAccountPage = new EditAccountPage(driver);
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
     }

    @Test(dependsOnMethods = "createAccount")
    public void deleteAccount() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login("user","bitnami");
        DashboardPage dashboardPage = new DashboardPage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.search(driver,wait,hover,accountName);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),accountName);
        searchResultsPage.clickOnFirstResult(wait);
        assertTrue(driver.getPageSource().toUpperCase().contains(accountName.toUpperCase()));
        EditAccountPage editAccountPage = new EditAccountPage(driver);
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
        editAccountPage.deleteAccount(driver, hover);
        System.out.println(accountName);
    }
}
